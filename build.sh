#!/usr/bin/env bash

echo "BUILDING MAIL IMAGE..."
podman build -f=Dockerfile.mail -t mail .  --build-arg buildtime_mail_passwd="${MAILPASSWD}"

