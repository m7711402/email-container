#!/bin/bash

BODY="Dear audience,

Add the body of your email here

Regards,

Author"

SUBJECT="E-mail subject"
RECIPIENT="To"
podman run --rm mail bash -c "echo \"${BODY}\" | mutt -s \"${SUBJECT}\" ${RECIPIENT}"

