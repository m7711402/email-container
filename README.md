# email-container
## Name
E-mail client in a container.  

## Description
Simple e-mail client (mutt) in a container.

## Installation
The following environment variable must be exported beforehand:  

```
MAILPASSWD
```

Then build the image as follows:  

```
./build.sh
```

## Usage
See ```send_email.sh```

## License
MIT license

